import imaplib
import email.header
import datetime
import base64
import os
import time
from dotenv import load_dotenv

# Your IMAP Settings
load_dotenv()
host = os.getenv("HOST")
user = os.getenv("EMAIL")
password = os.getenv("PASSWORD")
downloadAttachments = bool(os.getenv("DOWNLOADATTACHMENTS"))

def get_token(text, needle, closing):
    pos = text.find(needle)
    if pos == -1:
        return ""
    end = text.find(closing ,pos)
    if end == -1:
        return ""
    pos = pos + len(needle)
    return text[pos:end]

def get_payload(eml):
    payload = eml.get_payload()
    msg = ""
    while str(type(payload)) == "<class 'list'>":
        payload = payload[0]
        pldType = str(type(payload))
        if pldType == "<class 'email.message.Message'>":
            msg = payload.get_payload()
            if str(type(msg)) == "<class 'list'>":
                payload = msg
            else:
                break
        if pldType == "<class 'str'>":
            msg = payload
            break
        if pldType == "<class 'list'>":
            payload = msg
    if msg == "":
        msg = str(payload)
    return msg

def get_subject(subject):
    charMap = ""
    res = ""
    if subject.startswith("=?"):
        # strip encoding
        tokens = subject.split("?")
        charMap = tokens[1]
        if len(tokens[2]) > 1:
            res = tokens[2]
        else:
            res = tokens[3]
    else:
        end = subject.find("\n")
        if end != -1:
            res = str(subject[0:end])
    if res.find(" ") == -1 and res.find("_") == -1:
        res = str(base64.b64decode(res))

    return res

# Connect to the server
print(str(time.ctime()) + ' Connecting to ' + host)
mailBox = imaplib.IMAP4_SSL(host)

# Login to our account
mailBox.login(user, password)

boxList = mailBox.list()
# print(boxList)
spamWords = ["hookup", "Hookup", "sex", "Sex", "fuck", "Fuck", "lover", "Lover", "Horny", "horny", "Naughty", "naughty", "FLASHVERKOOP", "hacked", "hacker", "compromised", "UNDERGROUND", "hotgirls", "milf", "hotgirls"]

mailBox.select()
#searchQuery = '(SUBJECT "hookup")' # UNSEEN
searchQuery = '(SINCE "22-Apr-2019")'

allEmails = ""

result, data = mailBox.uid('search', None, searchQuery)
ids = data[0]
# list of uids
id_list = ids.split()

i = len(id_list)
for x in range(i):
    try:
        latest_email_uid = id_list[x]

        # fetch the email body (RFC822) for the given ID
        result, email_data = mailBox.uid('fetch', latest_email_uid, '(RFC822)')
        # I think I am fetching a bit too much here...

        raw_email = email_data[0][1]

        # converts byte literal to string removing b''
        raw_email_string = raw_email.decode('utf-8')
        email_message = email.message_from_string(raw_email_string)

        # downloading attachments
        for part in email_message.walk():
            # this part comes from the snipped I don't understand yet... 
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get('Content-Disposition') is None:
                continue
            fileName = part.get_filename()

            if bool(fileName) and downloadAttachments:
                filePath = os.getcwd() + os.sep + fileName
                if not os.path.isfile(filePath) :
                    fp = open(filePath, 'wb')
                    fp.write(part.get_payload(decode=True))
                    fp.close()

        subject = str(email_message).split("Subject: ", 1)[1].split("\nTo:", 1)[0]
        real_subject = ""
        contentType = get_token(subject, "Content-Type: ", ";")
        encoding = get_token(subject, "Content-Transfer-Encoding: ", "\n")
        real_subject = get_subject(subject)

        msg = get_payload(email_message)
        if encoding == 'base64' or msg.find(" ") == -1:
            msg = str(base64.b64decode(msg))

        spamIndexSubject = 0
        for spamWord in spamWords:
            if real_subject.__contains__(spamWord):
                spamIndexSubject = spamIndexSubject + 1
        spamIndexBody = 0
        for spamWord in spamWords:
            if msg.__contains__(spamWord):
                spamIndexBody = spamIndexBody + 1

        print('Email UID: ' + str(latest_email_uid))
        if spamIndexSubject > 0 or spamIndexBody > 0:
            print('Spam score: ' + str(spamIndexSubject + spamIndexBody) + ' => ' + str(spamIndexSubject) + '/' + str(spamIndexBody) + ' subject/body')
            print(' Subject: ' + real_subject)
            result = mailBox.uid('COPY', latest_email_uid, 'INBOX.spam')
            mov, data = mailBox.uid('STORE', latest_email_uid , '+FLAGS', '(\Deleted)')
            mailBox.expunge()
        else:
            print('Normal mail: ' + real_subject)
        #allEmails = allEmails + "\r\n------------ NEXT EMAIL UID: " + str(latest_email_uid) + " -----------\r\nSubject: " + real_subject + "\r\nBody: " + msg + "\r\n"
    except Exception as e:
        print(str(e.args))

#print(allEmails)
mailBox.close()
mailBox.logout()
